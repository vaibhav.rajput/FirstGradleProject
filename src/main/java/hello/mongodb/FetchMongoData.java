package hello.mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FetchMongoData {

    @Autowired
    private CustomerRepository repository;

    @RequestMapping("/fetchData")
    public List<Customer> fetchData(@RequestParam(value = "name", defaultValue = "World")
                                                String name) {
        return repository.findAll();
    }


}
